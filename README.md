# Congrès scientifiques et réseaux savants

Ce projet comprend les contenus et détails de la journée d'étude "Congrès scientifiques et réseaux savants" - qui abordera les enjeux et méthodes de traitements des données de participation à des évènements.

- Date : 8 Décembre 2021
- Lieu : Amphithéâtre de la MSH Paris Nord, 20 Avenue George Sand, La Plaine Saint Denis
- Inscriptions : [https://tinyurl.com/netconfscience](https://tinyurl.com/netconfscience)
