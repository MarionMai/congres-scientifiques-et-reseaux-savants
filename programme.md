## Programme 

*Journée d'étude du projet NETCONF et de la Transversalité Données et protocoles dans les Humanités Numériques*  

8 décembre 2021, à l'Amphithéâtre de la Maison des Sciences de l'Homme Paris Nord, 20 avenue George Sand, 93210 La Plaine Saint-Denis de 09h15 à 17h30  

- - -    

*09h15-09h30* - Mots d'introduction et brève présentation du projet NETCONF par
    *Bastien Bernela*, Laboratoire CRIEF, Poitiers

#### L'évènement scientifique au passé et au présent

- *09h30-10h10*  
     Établir la participation d'acteurs à des événements: sources croisées et approche probabiliste à partir d'un cas en archéologie préhistorique francophone (1969-1989) 
    *Sébastien Plutniak*, Centre Emile Durkheim, Bordeaux
- *10h10-10h40*  
     Routines et rituels dans l'organisation de conférences scientifiques en chimie. L'Annual Meeting de l'American Chemical Society  
    *Marianne Noël*, Laboratoire LISIS, Marne-la-Vallée

*10h40-10h50* - *Pause*  

#### Congrès et internationalisation

- *10h50-11h30*  
     Les associations et conférences scientifiques internationales comme agents de la circulation inégale des connaissances
    *Thibaud Boncourt, Susanne Koch et Elena Matviichuk*, Laboratoire CESSP, Paris, CWTS, Leyde, Pays-Bas et Global Landscapes Forum, Bonn, Allemagne

- *11h30-12h10*  
     Comparaison de réseaux de conférenciers en chimie verte et science politique: construction d'une approche commune 
    *François Briatte et Marion Maisonobe*, Laboratoire ESPOL, Lille et Géographie-cités, Paris  

*12h10-13h30* - *Déjeuner*  

#### Congrès de géographie et géographie des congrès  

- *13h30-14h10*  
     Les congrès allemands des géographes des décennies 1980-1990 : articulation d’approches et d’échelles
    *Mégane Fernandez*, Laboratoire Géographie-cités, Paris

- *14h10-14h50*  
     Discussion sur les transferts culturels et la spatialité des congrès  
    avec *Marie-Claire Robic*, Laboratoire Géographie-cités, Paris  

*14h50-15h00* - *Pause*  

#### Les congrès au tournant du XXe siècle

- *15h00-15h40*  
   Les Conférences scientifiques aux expositions universelles, 1889-1958   
    *Thomas Mougey*, Centre Alexandre Koyré, Paris

- *15h40-16h20*  
     Les congrès féminins et féministes entre 1878 et 1915. Un réseau transnational de la cause des femmes  
    *Alix Heiniger*, Département d'histoire contemporaine de l'Université de Fribourg, Fribourg, Suisse

#### Suivi des thématiques d'un congrès

- *16h20-17h00*  
     Cartographie d'un domaine émergent dans une grande conférence en oncologie  
    *Ale Hannud Abdo*, Laboratoire LISIS, Marne-la-Vallée  
    
*17h00-17h30* - Questions, bilans et perspectives de la journée  
